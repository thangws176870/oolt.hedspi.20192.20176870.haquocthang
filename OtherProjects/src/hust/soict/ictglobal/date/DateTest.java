package hust.soict.ictglobal.date;

import java.util.Scanner;

public class DateTest {

	public static void main(String[] args) {
		String strChoice;
		int choice=0;
		Scanner scanner = new Scanner(System.in);
		do {
			System.out.println("\n==Menu==\n1. What day is it?\n2. Insert a date with the format dd/MM/yyy\n3. Insert a date with the format FullMonthName OrdinalNumberDate Year\n4. Quit\n========\n");
			strChoice = scanner.nextLine();
			choice = Integer.parseInt(strChoice);
			switch(choice) {
			case 1:
				MyDate dateTest = new MyDate();
				//dateTest.print();
				String printChoice;
				System.out.println("Lua chon cach in:\n1. dd/mm/yyyy\n2. yy-mm-dd");
				printChoice = scanner.nextLine();
				int actualChoice = Integer.parseInt(printChoice);
				dateTest.print(actualChoice);
				break;
			case 2:
				System.out.println("Please insert a date with the format dd/MM/yyyy:\n");
				String input;
				input = scanner.next();
				String[] separatedDate = input.trim().split("/");
				int tempDay,tempMonth,tempYear;
				try {
					tempDay = Integer.parseInt(separatedDate[0]);
					tempMonth = Integer.parseInt(separatedDate[1]);
					tempYear = Integer.parseInt(separatedDate[2]);
				}catch(Exception se) {
					System.out.println("Invalid value inserted!\n");
					break;
				}
				
				if(tempYear==0) {
					System.out.println("Invalid year inserted!\n");
					break;
				}else if(tempMonth<1||tempMonth>12) {
						System.out.println("Invalid month inserted!\n");
						break;
					}else if(tempDay<1||tempDay>31||(tempMonth==2&&tempDay>29)) {
						System.out.println("Invalid day inserted!\n");
						break;
					}
				MyDate autoDateTest = new MyDate(tempDay,tempMonth,tempYear);
				
				/*dateTest.setDay(tempDay);
				dateTest.setMonth(tempMonth);
				dateTest.setYear(tempYear);*/
				
				System.out.println("These value has been succesfully inserted!\n");
				autoDateTest.print();
				break;
			case 3:
				MyDate ManualdateTest = new MyDate();
				ManualdateTest.accept();
				System.out.println("These value has been succesfully inserted!\n");
				ManualdateTest.print();
				break;
			case 4:
				System.out.println("Goodbye!");
				break;
			default:
				System.out.println("Only choose between 1~4!");
			}
		}while(choice!=4);
		scanner.close();
	}
}
