import javax.swing.JOptionPane;
public class DegreeEquations {
	public static void main(String[] args) {
		String strChoice;
		int choice = 0;

		do{
			strChoice = JOptionPane.showInputDialog(null,"Please choose one degree equation:\n1. The first degree equation with one variable\n2. The first degree equation with two variables\n3.The second degree equation with one variable\n4.Quit","Select the degree equation",JOptionPane.INFORMATION_MESSAGE);
			try{
				choice = Integer.parseInt(strChoice);
			}catch(NumberFormatException ex){
				JOptionPane.showMessageDialog(null, "Wrong number!");
				choice = 0;
			}
			switch(choice) {
			case 1:
				JOptionPane.showMessageDialog(null,"The FDE with one variable selected!");
				JOptionPane.showMessageDialog(null,"Please insert values of a, b, c for a simple FDE with one variable (ax + b = c):");
				String strA, strB, strC;
				double x;
				strA = JOptionPane.showInputDialog(null,"Please Insert value of a here: ","Insert a",JOptionPane.INFORMATION_MESSAGE);
				strB = JOptionPane.showInputDialog(null,"Please Insert value of b here: ","Insert b",JOptionPane.INFORMATION_MESSAGE);
				strC = JOptionPane.showInputDialog(null,"Please Insert value of c here: ","Insert c",JOptionPane.INFORMATION_MESSAGE);
				double a = Double.parseDouble(strA);
				double b = Double.parseDouble(strB);
				double c = Double.parseDouble(strC);
				if(a==0 & b==c){
					JOptionPane.showMessageDialog(null,"Moi x deu dung!");
					break;
				}else if(a==0 && b!=c){
					JOptionPane.showMessageDialog(null,"Vo nghiem!");
					break;
				}else{
				x = (c-b)/a;
				JOptionPane.showMessageDialog(null,"The value of the variable x is " + x);
				break;
				}
			case 2:
				JOptionPane.showMessageDialog(null,"The FDE with two variables selected!");
				JOptionPane.showMessageDialog(null,"Please insert values of a, b, c for a simple FDE with two variables (az + by = c):");
				double y,z;
				strA = JOptionPane.showInputDialog(null,"Please Insert value of a here: ","Insert a",JOptionPane.INFORMATION_MESSAGE);
				strB = JOptionPane.showInputDialog(null,"Please Insert value of b here: ","Insert b",JOptionPane.INFORMATION_MESSAGE);
				strC = JOptionPane.showInputDialog(null,"Please Insert value of c here: ","Insert c",JOptionPane.INFORMATION_MESSAGE);
				a = Double.parseDouble(strA);
				b = Double.parseDouble(strB);
				c = Double.parseDouble(strC);
				if (a!=0 & b!=0) JOptionPane.showMessageDialog(null,"Phuong trinh co dang y=" + (-a/b) + "z+" + c/b + "!");
				else if (a!=0 & b==0) JOptionPane.showMessageDialog(null,"Phuong trinh co dang z=" + c/a + "!");
				else if (a==0 & b==0) JOptionPane.showMessageDialog(null,"Phuong trinh co dang y=" + c/b + "!");
				else if (a==0 & b==0) JOptionPane.showMessageDialog(null,"Phuong trinh khong co nghiem x, y thoa man!");
				break;
			case 3:
				JOptionPane.showMessageDialog(null,"The SDE with one variable selected!");
				JOptionPane.showMessageDialog(null,"Please insert values of a, b, c for a simple SDE with one variable (ax^2 + bx + c = 0):");
				double x1, x2, delta, sqrtDelta;
				strA = JOptionPane.showInputDialog(null,"Please Insert value of a here: ","Insert a",JOptionPane.INFORMATION_MESSAGE);
				strB = JOptionPane.showInputDialog(null,"Please Insert value of b here: ","Insert b",JOptionPane.INFORMATION_MESSAGE);
				strC = JOptionPane.showInputDialog(null,"Please Insert value of c here: ","Insert c",JOptionPane.INFORMATION_MESSAGE);
				a = Double.parseDouble(strA);
				b = Double.parseDouble(strB);
				c = Double.parseDouble(strC);
				if (a==0) {
					if (b==0) {
						if (c==0) JOptionPane.showMessageDialog(null,"Moi x thoa man!");
						else JOptionPane.showMessageDialog(null,"Phuong tirnh vo nghiem!");
					} else {
						x1 = (c)/b;
						JOptionPane.showMessageDialog(null,"Phuong trinh co nghiem x=" + x1 + "!");
					}
				}
				else {
					delta = b*b-4*a*c;
					if (delta<0) JOptionPane.showMessageDialog(null,"Phuong tirnh vo nghiem!");
					else if (delta==0) {
						x1 = -b/(2*a);
						JOptionPane.showMessageDialog(null,"Phuong trinh co nghiem x=" + x1 + "!");
					}
					else {
						sqrtDelta = Math.sqrt(delta);
						x1 = (-b-sqrtDelta)/(2*a);
						x2 = (-b+sqrtDelta)/(2*a);
						JOptionPane.showMessageDialog(null,"Phuong trinh co nghiem x1=" + x1 + " va x2=" + x2 + "!");
					}
				}
				break;
			case 4:
				JOptionPane.showMessageDialog(null,"Quit!");
				break;
			default:
				JOptionPane.showMessageDialog(null,"Only choose between 1, 2 and 3!");
			}

		}while(choice!=4);
	}
}
