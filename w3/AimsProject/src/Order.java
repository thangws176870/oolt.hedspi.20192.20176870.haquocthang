import java.util.Scanner;
import java.time.format.DateTimeFormatter;
import java.text.DecimalFormat;
import java.time.LocalDateTime;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	private double totalCost = 0;
	private static int nbOrders = 0;
	private LocalDateTime dateOrdered;
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	
	public Order() {
		nbOrders=nbOrders+1;
		 if(nbOrders<MAX_LIMITTED_ORDERS) {
			 dateOrdered = LocalDateTime.now();
			 System.out.println("A new order list has been created!");
		 }
		 else if(nbOrders==MAX_LIMITTED_ORDERS) {
			 dateOrdered = LocalDateTime.now();
			 System.out.println("This is the last order list you can made today!");
		 }
		 else System.out.println("You cant create a new order! Maximum number of orders has been reached!");
	}

	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemsOrdered[qtyOrdered]=disc;
			qtyOrdered += 1;
			totalCost += disc.getCost();
			System.out.println("The disc " +disc.getTitle() + " has been added!");
		}
		else System.out.println("Your order list is full!");
	}
	
	
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		if((dvdList.length+qtyOrdered)>MAX_NUMBERS_ORDERED) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Cant add all items of this list into your cart! Do you want to add some of them and leave the rest?Y or N");
			String strChoice;
			strChoice = scanner.nextLine();
			if(strChoice=="Yes"||strChoice=="Y") {
				for(int i=0;(i<dvdList.length)&&(dvdList[i]!=null)&&(qtyOrdered<MAX_NUMBERS_ORDERED);i++) {
					itemsOrdered[qtyOrdered]=dvdList[i];
					qtyOrdered += 1;
					totalCost +=dvdList[i].getCost();
					System.out.println("The disc " +dvdList[i].getTitle() + " has been added!");
				}
			}else {
				System.out.println("You decide to leave this list!");
			}
		}else {
			for(int i=0;(i<dvdList.length)&&(dvdList[i]!=null)&&(qtyOrdered<MAX_NUMBERS_ORDERED);i++) {
				itemsOrdered[qtyOrdered]=dvdList[i];
				qtyOrdered += 1;
				totalCost +=dvdList[i].getCost();
				System.out.println("The disc " +dvdList[i].getTitle() + " has been added!");
			}
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		if((2+qtyOrdered)>MAX_NUMBERS_ORDERED) {
			System.out.println("Cant add these two items into your cart! Do you want to add the first one only?Y or N");
			Scanner scanner = new Scanner(System.in);
			String strChoice;
			strChoice = scanner.nextLine();
			if(strChoice=="Yes"||strChoice=="Y") {
				itemsOrdered[qtyOrdered]=dvd1;
				qtyOrdered += 1;
				totalCost +=dvd1.getCost();
				System.out.println("The disc " +dvd1.getTitle() + " has been added!");
				System.out.println("The disc " +dvd2.getTitle() + " has been dropped!");
			}else {
				System.out.println("You decide to drop these two discs!");
			}
		}else {
			itemsOrdered[qtyOrdered]=dvd1;
			qtyOrdered += 1;
			totalCost +=dvd1.getCost();
			System.out.println("The disc " +dvd1.getTitle() + " has been added!");
			
			itemsOrdered[qtyOrdered]=dvd2;
			qtyOrdered += 1;
			totalCost +=dvd2.getCost();
			System.out.println("The disc " +dvd2.getTitle() + " has been added!");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for(int i=0;i<qtyOrdered;i++) {
			if(itemsOrdered[i].getTitle().equals(disc.getTitle())==true) { //coi nhu khong co disc nao title giong ten nhau, neu khong thi them cac if khac
				for(int j=i;j<qtyOrdered-1;j++) {
					itemsOrdered[j]=itemsOrdered[j+1];
					itemsOrdered[j+1]=null;
					System.out.println("The disk " +disc.getTitle() + " has been removed!");
				}
				qtyOrdered = qtyOrdered - 1;
				totalCost-=disc.getCost();
			}
		}
	}
	
	
	
	public void printOrder() {
		int i;
		System.out.println("*********************Order************************");
		System.out.println("Date: "+dtf.format(dateOrdered));
		System.out.println("Ordered Items:");
		for(i=0; i<qtyOrdered; i++)
			System.out.println(i+1 + ". DVD - " + itemsOrdered[i].getTitle()+" - "+ itemsOrdered[i].getCategory()+" - "+ itemsOrdered[i].getDirector()+" - "+ itemsOrdered[i].getLength()+"' - "+ itemsOrdered[i].getCost()+"$");
		System.out.println("Total cost: "+getTotalCost()+"$");
		System.out.println("**************************************************");
	}	

	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public double getTotalCost() {
		DecimalFormat df = new DecimalFormat("#.##");
		totalCost = Double.parseDouble(df.format(totalCost));
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public LocalDateTime getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(LocalDateTime dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public static int getNbOrders() {
		return nbOrders;
	}
	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}
}
