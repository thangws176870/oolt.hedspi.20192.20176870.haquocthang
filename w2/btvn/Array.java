package w2;

import java.util.Scanner;
import java.util.*;
import javax.swing.*;

public class Array {
	public static void main(String[] agrs) {
		int[] iArray = {3,8,6,2,1};
		String strChoice;
		int choice = 0;
		Scanner scanner = new Scanner(System.in);
		do {
			strChoice = JOptionPane.showInputDialog(null,"Please pick one choice here:\n"
					+ "1. Show items of array\n"
					+ "2. Sort the array\n"
					+ "3. Caculate the sum of array\n"
					+ "4. Caculate the avarage value of array\n"
					+ "5. Quit","Select the degree equation",JOptionPane.INFORMATION_MESSAGE);
			choice = Integer.parseInt(strChoice);
			switch(choice) {
			case 1:
				String arrayList = null;
				for(int i=0;i<(iArray.length);i++) {
					arrayList += iArray[i];
					if(i<(iArray.length-1)) arrayList += ",";
				}
				JOptionPane.showMessageDialog(null, arrayList);
				break;
			case 2:
				Arrays.sort(iArray);
				JOptionPane.showMessageDialog(null,Arrays.toString(iArray));
				break;
			case 3:
				int sum1=0;
				for(int i=0;i<iArray.length;i++) {
					sum1 += iArray[i];
				}
				JOptionPane.showMessageDialog(null,"The sum of this array is " + sum1 + "!");
				break;
			case 4:
				int sum2=0;
				for(int i=0;i<iArray.length;i++) {
					sum2 += iArray[i];
				}
				int avg=sum2/iArray.length;
				JOptionPane.showMessageDialog(null,"The avarage sum of this array is " + avg + "!");
				break;
			case 5:
				JOptionPane.showMessageDialog(null,"Goodbye!");
				break;
			default: JOptionPane.showMessageDialog(null, "Please only choose between 1~5!");
			}
		}
		while(choice!=5);
		scanner.close();
	}
}