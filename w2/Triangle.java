package w2;
import java.util.*;
public class Triangle {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("How many rows do you want the triangle to have?");
		int n = scanner.nextInt();
		int rows = n;
		for (int i = 1; i <= rows; i++) { 
            for (int j = rows; j > i; j--) { 
                System.out.print(" "); 
            }
            for (int j = 1; j <= i; j++) { 
                System.out.print("* "); 
            } 
            System.out.println();
        }
		scanner.close();
	}
	
}
