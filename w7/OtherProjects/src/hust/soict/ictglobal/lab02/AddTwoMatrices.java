package hust.soict.ictglobal.lab02;

public class AddTwoMatrices {
	public static void main(String[] agrs) {
		int[][] matrix1 = {{1,2,3,4,5},{6,7,8,9,10}};
		int[][] matrix2 = {{9,18,27,36,45},{54,63,72,81,90}};
		int[][] matrix3 = new int[matrix1.length][matrix1[0].length];
		for(int i=0;i<matrix1.length;i++) {
			for(int j=0;j<matrix1[i].length;j++) {
				matrix3[i][j] = matrix1[i][j] + matrix2[i][j];
			}
		}
		for(int i = 0;i<matrix1.length;i++) {
			for(int j = 0;j<matrix1[i].length;j++) {
				System.out.print(matrix3[i][j]);
				if(j<(matrix1[i].length-1)) System.out.print("\t");
				if(j==(matrix1[i].length-1)) System.out.println();
				}
		}
	}
}
