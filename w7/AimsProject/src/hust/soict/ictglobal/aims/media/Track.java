package hust.soict.ictglobal.aims.media;

public class Track implements Playable {
	private int length;
	private String title;
	
	public Track () {
		// TODO Auto-generated constructor stub
	}
	
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
		}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
