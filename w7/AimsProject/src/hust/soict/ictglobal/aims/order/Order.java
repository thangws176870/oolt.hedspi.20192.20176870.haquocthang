package hust.soict.ictglobal.aims.order;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import hust.soict.ictglobal.aims.media.Media;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private static int nbOrders = 0;
	private LocalDateTime dateOrdered;
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	
	public void addMedia(Media media) {
		if(itemsOrdered.size()<MAX_NUMBERS_ORDERED) {
			itemsOrdered.add(media);
			System.out.println(media.getTitle() + " has been added!");
		}else {
			System.out.println(media.getTitle() + " can't be added! Your cart is full!");
		}
	}
	
	public void removeMedia(Media media) {
		if(itemsOrdered.contains(media)) {
			itemsOrdered.remove(media);
			System.out.println(media.getTitle() + " has been removed!");
		}else {
			System.out.println(media.getTitle() + " is not in your cart!");
		}
	}
	
	public void removeMedia(int Id) {
		int flag = 0;
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if(itemsOrdered.get(i).getId()==Id) {
				System.out.println(itemsOrdered.get(i).getTitle() + " has been removed!");
				itemsOrdered.remove(i);
				flag = 1;
			}
		}
		if (flag == 0) System.out.println(itemsOrdered.get(Id).getTitle() + " is not in your cart!");
	}
	
	public Order() {
		nbOrders=nbOrders+1;
		 if(nbOrders<MAX_LIMITTED_ORDERS) {
			 dateOrdered = LocalDateTime.now();
			 System.out.println("A new order list has been created!");
		 }
		 else if(nbOrders==MAX_LIMITTED_ORDERS) {
			 dateOrdered = LocalDateTime.now();
			 System.out.println("This is the last order list you can make today!");
		 }
		 else System.out.println("You cant create a new order! Maximum number of orders has been reached!");
	}

	public Media getALuckyItem() {
            int rand = (int)(Math.random() * itemsOrdered.size());
            itemsOrdered.get(rand).setCost(0);
            return itemsOrdered.get(rand);
	}
	
	public void printOrder() {
		int i;
		System.out.println("*********************Order************************");
		System.out.println("Date: "+dtf.format(dateOrdered));
		System.out.println("Ordered Items:");
		for(i=0; i<itemsOrdered.size(); i++)
			System.out.println(i+1 + ". ID: "+itemsOrdered.get(i).getId()+" - DVD - " + itemsOrdered.get(i).getTitle()+" - "+ itemsOrdered.get(i).getCategory()+" - "+ /*itemsOrdered.get(i).getDirector()+" - "+ itemsOrdered.get(i).getLength()+"' - "*/+ itemsOrdered.get(i).getCost()+"$");
		System.out.println("Total cost: "+TotalCost()+"$");
		System.out.println("**************************************************");
	}	

	public float TotalCost() {
		float totalCost = 0;
		for(int i=0; i<itemsOrdered.size(); i++) {
			totalCost += itemsOrdered.get(i).getCost();
		}
		DecimalFormat df = new DecimalFormat("#.##");
		totalCost = Float.parseFloat(df.format(totalCost));
		return totalCost;
	}
	public LocalDateTime getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(LocalDateTime dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public static int getNbOrders() {
		return nbOrders;
	}
	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}

	public int getSize() {
		int size = itemsOrdered.size();
		return size;
	}
}
