package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Book extends Media implements Comparable {
	
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new HashMap<String, Integer>();
	
	public Book() {
	}
	
	public Book(String title) {
	    super(title);
	  }

	  public Book(String title, String category) {
	    super(title, category);
	  }

	  public Book(String title, String category, List<String> authors) {
	    super(title, category);
	    this.authors = authors;
	  }
	  
	  public Book(String title, String category, List<String> authors, int id, float cost) {
		    super(title, category, id, cost);
		    this.authors = authors;
		  }
	
	public void addAuthor(String authorName) {
		if(authors.contains(authorName)) {
			System.out.println("This author is already in the list!");
		}else {
			authors.add(authorName);
			System.out.println("This author has been added!");
		}
	}
	
	public void removeAuthor(String authorName) {
		if(authors.contains(authorName)) {
			authors.remove(authorName);
			System.out.println("This author has been removed!");
		}else {
			System.out.println("This author is not in the list!");
		}
	}
	
	public int compareTo(Object obj) {
		return this.getTitle().compareToIgnoreCase(((Book)obj).getTitle());
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

}
