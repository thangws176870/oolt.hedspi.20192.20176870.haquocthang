package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class CompactDisc extends Disc implements Playable, Comparable {
	private String artist;
	private int length;
	private List<Track> tracks = new ArrayList<Track>();
	
	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}
	
	public void addTrack(Track track) {
		if(this.tracks.contains(track)) {
			System.out.println("The track "+track.getTitle()+" is already in the list of tracks!");
		}else {
			this.tracks.add(track);
			System.out.println("The track "+track.getTitle()+" has been add to the list of tracks!");
		}
	}
	
	public void removeTrack(Track track) {
		if(this.tracks.contains(track)) {
			this.tracks.add(track);
			System.out.println("The track "+track.getTitle()+" has been remove from the list of tracks!");
		}else {
			System.out.println("The track "+track.getTitle()+" is not in the list of tracks!");
		}
	}
	
	public int getLength() {
		for(int i=0; i<tracks.size();i++) {
			length += tracks.get(i).getLength();
		}
		return length;
	}
	
	public void play() {
	    System.out.println("Playing " + this.getArtist() + "'s CD"); //this CD's artist
	    System.out.println("This CD is "+this.getLength()+" long"); //this CD's total length
	    for (int i = 0; i < tracks.size(); i++) {
	      tracks.get(i).play();  //contents of this CD
	    }
	  }
	
	public int compareTo(Object obj) {
		if(this.tracks.size()>((CompactDisc)obj).tracks.size())
			return 1;
		else if(this.tracks.size()<((CompactDisc)obj).tracks.size())
			return -1;
		else if(this.length>((CompactDisc)obj).length)
			return 1;
		else if(this.length<((CompactDisc)obj).length)
			return -1;
		return this.getTitle().compareToIgnoreCase(((CompactDisc)obj).getTitle());
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

}
