package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		DigitalVideoDisc[] dvdList=new DigitalVideoDisc[10];
		Order anOrder = new Order();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addMedia(dvd1);
		dvdList[0]=dvd1;
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addMedia(dvd2);
		dvdList[1]=dvd2;
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		anOrder.addMedia(dvd3);
		dvdList[2]=dvd3;
		
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Fast Five");
		dvd4.setCategory("Action");
		dvd4.setCost(12.63f);
		dvd4.setDirector("Justin Lin");
		dvd4.setLength(88);
		anOrder.addMedia(dvd4);
		dvdList[3]=dvd4;
		
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Taxi Driver");
		dvd5.setCategory("Science Fiction");
		dvd5.setCost(28.12f);
		dvd5.setDirector("Martin Scorsese");
		dvd5.setLength(120);
		anOrder.addMedia(dvd5);
		dvdList[4]=dvd5;
		
		DigitalVideoDisc dvd6 = new DigitalVideoDisc("The Martian");
		dvd6.setCategory("Sci-fiction");
		dvd6.setCost(32.22f);
		dvd6.setDirector("Ridley Sott");
		dvd6.setLength(91);
		anOrder.addMedia(dvd6);
		dvdList[5]=dvd6;
		
		DigitalVideoDisc dvd7 = new DigitalVideoDisc("Interstellar");
		dvd7.setCategory("Action");
		dvd7.setCost(49.52f);
		dvd7.setDirector("Christopher Nolan");
		dvd7.setLength(42);
		anOrder.addMedia(dvd7);
		dvdList[6]=dvd7;
		
		/*DigitalVideoDisc dvd8 = new DigitalVideoDisc("Edward Scissorhands");
		dvd8.setCategory("Drama");
		dvd8.setCost(48.12f);
		dvd8.setDirector("Tim Burton");
		dvd8.setLength(184);
		anOrder.addDigitalVideoDisc(dvd8);
		dvdList[7]=dvd8;
		
		DigitalVideoDisc dvd9 = new DigitalVideoDisc("James Cameron");
		dvd9.setCategory("Sci-fiction");
		dvd9.setCost(22.22f);
		dvd9.setDirector("James Cameron");
		dvd9.setLength(190);
		anOrder.addDigitalVideoDisc(dvd9);
		dvdList[8]=dvd9;*/
		
		//Test search()
		
		
		System.out.println("What movie are you looking for? Enter a title:");
		String inputSearch = scanner.nextLine();
		try {
			for(int i=0;i<=dvdList.length;i++) {
				boolean check = dvdList[i].search(inputSearch);
				if(check) {
					System.out.println("This dics is No."+(i+1)+" in your cart!");
					break;
				}
			}
		}catch(NullPointerException e)	{
			System.out.println("The disc \""+inputSearch+"\" is not in your cart!");
		}
		
		//The bill before "lucky discount"
		anOrder.printOrder();
		//The bill after "lucky discount"
		System.out.println("\nWait a second! You will get a free disc!\n");
		try {
			Media freeItem = anOrder.getALuckyItem();
			System.out.println("The disc "+freeItem.getTitle()+" is free for you!");
		}catch(NullPointerException e) {
			System.out.println("Unfortunately you dont get any free disc!"); //works if change the random value to 1~MAX_NUMBERS_ORDERED in getAluckyItem()
		}
		anOrder.printOrder();
	}

}
