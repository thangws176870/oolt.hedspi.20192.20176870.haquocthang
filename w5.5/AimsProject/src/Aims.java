public class Aims {
	public static void main(String[] args) {
		
		// Test addDigitalVideoDisc(DigitalVideoDisc disc)
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);
		
		
		System.out.println();
		System.out.println("Your cart has " +  anOrder.getQtyOrdered() + " item(s)."); //????? sao lai khong cho vao class Order luon nhi :v
		anOrder.printOrder();
		System.out.println();
		anOrder.removeDigitalVideoDisc(dvd2);
		System.out.println();
		System.out.println("Your cart has " +  anOrder.getQtyOrdered() + " item(s).");
		anOrder.printOrder();
		
		System.out.println("=============================================================");
		
		//Test addDigitalVideoDisc(DigitalVideoDisc [] dvdList)
		Order secondOrder = new Order();
		DigitalVideoDisc[] dvdList=new DigitalVideoDisc[10]; 
		
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Stars");
		dvd4.setCategory("Science");
		dvd4.setCost(21.05f);
		dvd4.setDirector("James Bay");
		dvd4.setLength(124);
		dvdList[0]=dvd4;
		
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("CMT8");
		dvd5.setCategory("History");
		dvd5.setCost(20.99f);
		dvd5.setDirector("Quoc Thang");
		dvd5.setLength(90);
		dvdList[1]=dvd5;
		
		secondOrder.addDigitalVideoDisc(dvdList);
		System.out.println();
		System.out.println("Your cart has " +  secondOrder.getQtyOrdered() + " item(s).");
		secondOrder.printOrder();
		System.out.println();
		
		System.out.println("=============================================================");
		
		//Test addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
		Order thirdOrder = new Order();
		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Forrest Gump");
		dvd6.setCategory("Motivation");
		dvd6.setCost(31.31f);
		dvd6.setDirector("Robert Zemeckis");
		dvd6.setLength(200);
		
		DigitalVideoDisc dvd7 = new DigitalVideoDisc("Dunkirk");
		dvd7.setCategory("History");
		dvd7.setCost(42.22f);
		dvd7.setDirector("Christopher Nolan");
		dvd7.setLength(180);
		
		thirdOrder.addDigitalVideoDisc(dvd6, dvd7);
		System.out.println();
		System.out.println("Your cart has " +  thirdOrder.getQtyOrdered() + " item(s).");
		thirdOrder.printOrder();
		System.out.println();
		
		System.out.println("=============================================================");
		
		//Test nbOrder
		System.out.print("The number of made orders: "+Order.getNbOrders());
	}
}
