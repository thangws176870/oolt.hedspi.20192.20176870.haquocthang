package hust.soict.ictglobal.lab01;

import javax.swing.JOptionPane;
public class TwoNumbers {
	public static void main(String[] args) {
		String strNum1, strNum2;
		double sum, difference, product, quoient;
		String strNotification = "You've just entered: ";
		strNum1 = JOptionPane.showInputDialog(null,"Please input the first number: ","Input the first number",JOptionPane.INFORMATION_MESSAGE);
		double num1 = Double.parseDouble(strNum1);
		strNotification += strNum1 + " and ";
		strNum2 = JOptionPane.showInputDialog(null,"Please input the second number: ","Input the second number",JOptionPane.INFORMATION_MESSAGE);
		double num2 = Double.parseDouble(strNum2);
		strNotification += strNum2 + ". ";
		sum = num1+num2;
		difference = num1-num2;
		product = num1*num2;
		if (num2 == 0) {

			strNotification += "The sum is " + sum + ", the difference is " + difference + ", the product is 0 and " + num1 + " can't be divided by 0.";
		}
		else{
			quoient = num1/num2;
			strNotification += "The sum is " + sum + ", the difference is " + difference + ", the product is " + product + " and the quoient is " + quoient + ".";
		}
		JOptionPane.showMessageDialog(null,strNotification,"Two numbers and their infomations",JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}