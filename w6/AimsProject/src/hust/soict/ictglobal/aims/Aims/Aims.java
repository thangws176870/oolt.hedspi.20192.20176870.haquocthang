package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;
import java.util.ArrayList;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {
	public static void main(String[] args) {
		ArrayList<Order> ordersList = new ArrayList<Order>();
		DigitalVideoDisc[] dvdList=new DigitalVideoDisc[10]; 
		discList(dvdList);
		Scanner scanner = new Scanner(System.in);
		int choice;
		do {
			showMenu();
			choice = scanner.nextInt();
			switch(choice) {
			case 1:
				System.out.println("\n==========1. Create order==========\n");
				Order anOrder = new Order();
				if(Order.getNbOrders()<=Order.MAX_NUMBERS_ORDERED) ordersList.add(anOrder);
				System.out.println("\n==================================\n");
				break;
			case 2:
				System.out.println("\n===========2. Add an item============\n");
				int itemId;
				showDiscList(dvdList);
				System.out.print("Pick a item you want by its ID: ");
				itemId = scanner.nextInt();
				int flag1 = 0;
				try {
					for(int i = 0; i<dvdList.length; i++) {
						if(dvdList[i].getId()==itemId) {
							ordersList.get(ordersList.size() - 1).addMedia(dvdList[i]);
							flag1 = 1;
						}
					}
					if(flag1 == 0) System.out.println("The media with Id "+ itemId +" is not in our list!");
				}catch(IndexOutOfBoundsException e) {
					System.out.println("You must create an order first!");
				}catch(NullPointerException e){
					//
				}
				System.out.println("\n==================================\n");
				break;
			case 3:
				System.out.println("\n==========2. Remove an item========\n");
				int RemoveItemId;
				System.out.print("Pick a item you want to remove by its ID: ");
				RemoveItemId = scanner.nextInt();
				ordersList.get(ordersList.size() - 1).removeMedia(RemoveItemId);
				System.out.println("\n==================================\n");
				break;
			case 4:
				System.out.println("\n==========4. Print order==========\n");
				ordersList.get(ordersList.size() - 1).printOrder();
				System.out.println("\n==================================\n");
				break;
			case 0:
				System.out.println("\n=============0. Quit==============\n");
				System.out.println("Quitting...");
				System.out.println("\n==================================\n");
			default:
					System.out.println("\nWrong input!\n");
			}
		} while(choice!=0);
		
	}
	
	public static void discList(DigitalVideoDisc[] dvdList) {
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		dvd1.setId(0);

		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		dvd2.setId(1);

		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		dvd3.setId(2);
		
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Bad boys");
		dvd4.setCategory("Comedy");
		dvd4.setCost(21.05f);
		dvd4.setDirector("Micheal Bay");
		dvd4.setLength(124);
		dvd4.setId(3);

		DigitalVideoDisc dvd5 = new DigitalVideoDisc("CMT8");
		dvd5.setCategory("History");
		dvd5.setCost(20.99f);
		dvd5.setDirector("Quoc Thang");
		dvd5.setLength(80);
		dvd5.setId(4);

		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Forrest Gump");
		dvd6.setCategory("Motivation");
		dvd6.setCost(31.31f);
		dvd6.setDirector("Robert Zemeckis");
		dvd6.setLength(200);
		dvd6.setId(5);

		DigitalVideoDisc dvd7 = new DigitalVideoDisc("Dunkirk");
		dvd7.setCategory("History");
		dvd7.setCost(42.22f);
		dvd7.setDirector("Christopher Nolan");
		dvd7.setLength(180);
		dvd7.setId(6);
		
		dvdList[0]=dvd1;
		dvdList[1]=dvd2;
		dvdList[2]=dvd3;
		dvdList[3]=dvd4;
		dvdList[4]=dvd5;
		dvdList[5]=dvd6;
		dvdList[6]=dvd7;
	}
	
	public static void showDiscList(DigitalVideoDisc[] dvdList) {
		System.out.println("============Our Disk List============");
		try {
		for(int i = 0; i< dvdList.length; i++) {
			//System.out.println("Title: " + dvdList[i].getTitle()+ "\t\t\t\t\t Id: " + dvdList[i].getId());
			System.out.printf("%-15s %-10d\n", dvdList[i].getTitle(), dvdList[i].getId());
		}
		}catch(NullPointerException e) {
		}
		System.out.println("====================================");
	}
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.print("Please choose a number: 0-1-2-3-4: ");
		}
}

/*Order anOrder = new Order();
DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
dvd1.setCategory("Animation");
dvd1.setCost(19.95f);
dvd1.setDirector("Roger Allers");
dvd1.setLength(87);
anOrder.addMedia(dvd1);

DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
dvd2.setCategory("Science Fiction");
dvd2.setCost(24.95f);
dvd2.setDirector("George Lucas");
dvd2.setLength(124);
anOrder.addMedia(dvd2);

DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
dvd3.setCategory("Animation");
dvd3.setCost(18.99f);
dvd3.setDirector("John Musker");
dvd3.setLength(90);
anOrder.addMedia(dvd3);

//Test removeDigitalVideoDisc
anOrder.removeMedia(dvd2);
System.out.println();
System.out.println("Your cart has " +  anOrder.getSize() + " item(s).");
anOrder.printOrder();

System.out.println("=============================================================");*/

/*DigitalVideoDisc dvd4 = new DigitalVideoDisc("Bad boys");
dvd4.setCategory("Comedy");
dvd4.setCost(21.05f);
dvd4.setDirector("Micheal Bay");
dvd4.setLength(124);

DigitalVideoDisc dvd5 = new DigitalVideoDisc("CMT8");
dvd5.setCategory("History");
dvd5.setCost(20.99f);
dvd5.setDirector("Quoc Thang");
dvd5.setLength(90);

DigitalVideoDisc dvd6 = new DigitalVideoDisc("Forrest Gump");
dvd6.setCategory("Motivation");
dvd6.setCost(31.31f);
dvd6.setDirector("Robert Zemeckis");
dvd6.setLength(200);

DigitalVideoDisc dvd7 = new DigitalVideoDisc("Dunkirk");
dvd7.setCategory("History");
dvd7.setCost(42.22f);
dvd7.setDirector("Christopher Nolan");
dvd7.setLength(180);*/
