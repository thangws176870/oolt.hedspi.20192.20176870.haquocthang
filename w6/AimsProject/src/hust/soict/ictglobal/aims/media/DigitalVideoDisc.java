package hust.soict.ictglobal.aims.media;

import java.util.Arrays;

public class DigitalVideoDisc extends Media {
	
	private String director;
	private int length;
	
	public DigitalVideoDisc(String title) {
		super(title);
	}
	
	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}
	
	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}
	
	public DigitalVideoDisc(String title, String category, String director, int length) {
		super(title, category);
		this.director = director;
		this.length = length;
	}
	
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	
	public boolean search(String title) {
		String[] arrTitle = title.trim().split(" ");
		String[] arrThisTitle = this.getTitle().trim().split(" ");
		if(arrTitle.length==arrThisTitle.length) {
			Arrays.sort(arrTitle);
			Arrays.sort(arrThisTitle);
			for (int i=0; i<arrTitle.length; i++) {
				if (!arrTitle[i].equalsIgnoreCase(arrThisTitle[i]))
					return false;
				}
					return true;
		}else return false;
	}
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	
}
