package hust.soict.ictglobal.lab02;

import java.util.*;

public class NumberOfDaysOfAMonth {
	public static void main(String[] args) {
		int month, year = 2019;
		Scanner scanner = new Scanner(System.in);
		do {
			System.out.println("Insert month:");
			month = scanner.nextInt();
		}while(month<=1||month>=12);
		do {
			System.out.println("Insert year:");
			year = scanner.nextInt();
		}while(year<1);
		if (month==1||month==3||month==5||month==7||month==8||month==10||month==12) {
			System.out.println("Thang " + month + " cua nam " + year + " co 31 ngay!");
		}else if (month==4||month==6||month==9||month==11) {
			System.out.println("Thang " + month + " cua nam " + year + " co 30 ngay!");
		}else {
			if (year%4 ==0) System.out.println("Nam " + year + " la nam nhuan nen thang " + month + " co 29 ngay!");
			else System.out.println("Thang " + month + " cua nam " + year + " co 28 ngay!");
		}
		scanner.close();
	}
}
