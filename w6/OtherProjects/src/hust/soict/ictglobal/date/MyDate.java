package hust.soict.ictglobal.date;


import java.util.Calendar;
import java.util.Scanner;
 
public class MyDate {
	private int day, month, year;
	String monthOfYear[] = {"START","January","February","March","April","May","June","July","August","September","October","November","December","Invalid Month"};
	
	public MyDate() {
		Calendar calendar = Calendar.getInstance();
		this.day = calendar.get(Calendar.DATE);
		this.month = calendar.get(Calendar.MONTH);
		this.year = calendar.get(Calendar.YEAR);
	}
	
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String strDate) {
		strDateConverter(strDate);
	}
	
	public boolean strDateConverter(String strDate){
		int i = 0;
		String[] separatedDate = strDate.trim().split("\\s+");
		while(separatedDate[0].equalsIgnoreCase(monthOfYear[i])==false) {
			if(i<=12) {
			i=i+1;
			} else break;
		}
		if(i>12) {
			System.out.println("Invalid month!\n");
			return false;
		}else {
			this.month=i;
		}
		String strDay=separatedDate[1].replaceAll("st|nd|rd|th", "");
		int tempDay;
		try {
		tempDay = Integer.parseInt(strDay);
		}catch(Exception se){
			System.out.println("Invalid day!\n");
			return false;
		}
		if(tempDay<1||tempDay>31||(this.month==2&&tempDay>29)) {
			System.out.println("Invalid day!\n");
			return false;
		}
		else {
			this.day = tempDay;
		}
		int tempYear;
		try{
			tempYear = Integer.parseInt(separatedDate[2]);
		}catch(Exception se) {
			System.out.println("Invalid year!\n");
			return false;
		}
		if(tempYear==0) {
			System.out.println("Invalid year! There's not year 0, negative value of year should be consider as BC year!\n");
			return false;
		}
		else {
			this.year = tempYear;
		}
		return true;
	}
	
	public void accept() {
		String input;
		boolean flag = true;
		Scanner scanner = new Scanner(System.in);
		do {
			System.out.println("Please insert a date (e.g. “February 18th 2019”): ");
			input = scanner.nextLine();
			flag = strDateConverter(input);
		}while(!flag);
	}
	
	public void print() {
		System.out.println("The current SAVED data of date is: "+day+"/"+month+"/"+year+".\n");
	}
	
	public void print(int choice) {
		switch(choice) {
		case 1:
			System.out.println("The current SAVED data of date is: "+day+"/"+month+"/"+year+".\n");
			break;
		case 2:
			System.out.println("The current SAVED data of date is: "+String.valueOf(year).substring(2)+"-"+month+"-"+day+".\n");
			break;
		case 3:
			default:
				System.out.println("Chon sai format!");
				break;
		}
	}
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(day<1&&day>31) {
			System.out.println("Invalid value of day!\n");
		}else
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if(month<1&&month>12) {
			System.out.println("Invalid value of month!\n");
		}else
		this.month = month;
	}
	public void setMonth(String strMonth) {
		int i = 0;
		while(strMonth.equalsIgnoreCase(monthOfYear[i])==false) {
			if(i<=12) {
			i=i+1;
			} else break;
		}
		if(i>12) {
			System.out.println("Invalid month!\n");
			return;
		}else {
			this.month=i;
		}
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if(year==0) { //Khong co nam 0, neu nam la am thi coi nhu truoc Cong Nguyen
			System.out.println("Invalid value of year!\n");
		}else
		this.year = year;
	}
}